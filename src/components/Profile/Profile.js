import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
//making sure Redirect imports from react-router-dom and not react-router
import { Link, Redirect } from "react-router-dom"
import AppContainer from "../../hoc/AppContainer"
import { sessionLogoutAction } from "../../store/actions/sessionActions"
import {
    translationDeleteAction,
    translationGetAction,
} from "../../store/actions/translationActions"

const Profile = () => {
    //useDispatch() is a hook from react-redux library
    //to dispatch functions
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(translationGetAction())
    }, [])

    //get loggedIn variable from sessionReducer.js
    const { loggedIn } = useSelector((state) => state.sessionReducer)

    const { translationLog } = useSelector((state) => state.translationReducer)

    const onClickClear = () => dispatch(translationDeleteAction())

    const onClickLogout = () => {
        dispatch(translationDeleteAction())
        dispatch(sessionLogoutAction())
    }

    return (
        <>
            {/* IF not loggedIn, redirect to start */}
            {!loggedIn && <Redirect to="/" />}

            {/* IF loggedIn, render this */}
            {loggedIn && (
                <AppContainer>
                    <h1>PROFILE</h1>
                    <h2>Translations...</h2>
                    <div>
                        {translationLog &&
                            translationLog.map((translation, index) => (
                                <div key={index}>
                                    <li>{translation}</li>
                                </div>
                            ))}
                    </div>

                    <button
                        onClick={onClickClear}
                        className="btn btn-primary btn-lg"
                    >
                        Clear log
                    </button>

                    <Link to="/">
                        <button
                            onClick={onClickLogout}
                            className="btn btn-primary btn-lg"
                        >
                            Logout
                        </button>
                    </Link>
                </AppContainer>
            )}
        </>
    )
}

export default Profile
