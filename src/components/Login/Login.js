import { useState } from "react"
import { useDispatch, useSelector } from "react-redux"
//making sure Redirect imports from react-router-dom and not react-router
import { Redirect } from "react-router-dom"
import AppContainer from "../../hoc/AppContainer"
import { loginAttemptAction } from "../../store/actions/loginActions"

const Login = () => {
    //useDispatch() is a hook from react-redux library
    //to dispatch functions
    const dispatch = useDispatch()

    //using hook to destructure loginError and loginAttempting
    const { loginError, loginAttempting } = useSelector(
        (state) => state.loginReducer
    )

    //get loggedIn variable from sessionReducer.js
    const { loggedIn } = useSelector((state) => state.sessionReducer)

    //useState() hook receives default state argument
    //using array destructuring to get default credentials and setCredentials from useState()
    //only valid in functional components
    //setting a local state:
    const [credentials, setCredentials] = useState({
        username: "",
    })

    const onInputChange = (event) => {
        setCredentials({
            //make copy of credentials from state into new object
            ...credentials,
            [event.target.id]: event.target.value,
        })
    }

    const onFormSubmit = (event) => {
        //form submission restarts app by default,
        //overriding default behaviour
        event.preventDefault()
        dispatch(loginAttemptAction(credentials))
    }

    return (
        <>
            {/* using loggedIn variable fetched from loginReducer as logical flag */}
            {/* IF loggedIn, redirect to timeline page */}
            {loggedIn && <Redirect to="/translation" />}

            {/* IF !loggedIn, render this (flag prevents flashes of this page when logged in) */}
            {!loggedIn && (
                <AppContainer>
                    {/* form becomes child of AppContainer */}
                    <form className="mt-3 mb-3" onSubmit={onFormSubmit}>
                        {/* mt-3 = bootstrap top margin size 3 */}

                        <h1>Login to Lost in Translation</h1>

                        {/* mb-3 = bootstrap bottom margin size 3 */}
                        {/* jsx syntax class = className */}
                        {/* using bootstrap classes form-label and form-control */}
                        <div className="mb-3">
                            {/* jsx syntax for = htmlFor */}
                            <label htmlFor="username" className="form-label">
                                Username
                            </label>
                            <input
                                id="username"
                                type="text"
                                placerholder="Enter your username"
                                className="form-control"
                                onChange={onInputChange}
                            />
                        </div>

                        {/* button is set to submit the form */}
                        <button
                            type="submit"
                            className="btn btn-primary btn-lg"
                        >
                            Login
                        </button>

                        {/* IF loginError exists, follow up with jsx code */}
                    </form>

                    {loginAttempting && <p>Trying to login...</p>}

                    {loginError && (
                        <div className="alert alert-danger" role="alert">
                            <h4>Unsuccessful</h4>
                            {/* mb-0 no margin at bottom */}
                            <p className="mb-0">{loginError}</p>
                        </div>
                    )}
                </AppContainer>
            )}
        </>
    )
}

export default Login
