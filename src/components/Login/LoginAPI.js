import { handleFirstResponse } from "../../utils/apiUtils"

export const LoginAPI = {
    login(username) {
        return fetch("http://localhost:8000/user", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(username),
        }).then(handleFirstResponse)
    },
}
