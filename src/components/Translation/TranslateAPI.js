import { handleFirstResponse } from "../../utils/apiUtils"

export const TranslateAPI = {
    addTranslation([username, translation]) {
        console.log(translation)
        return fetch("http://localhost:8000/translation", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                user: username,
                text: translation,
            }),
        }).then(handleFirstResponse)
    },

    getTranslation() {
        return fetch("http://localhost:8000/translation", {})
            .then((response) => {
                if (response.status !== 200) {
                    throw new Error("Could not fetch logged translations.")
                }
                return response.json()
            })
            .then((response) => {
                if (response.values.length > 10) {
                    return response.values.slice(-10)
                } else return response.values
            })
            .catch((error) => {
                return error
            })
    },

    deleteTranslation() {
        return fetch("http://localhost:8000/translation", {
            method: "DELETE",
        })
    },
}
