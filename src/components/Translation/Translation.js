import { useState } from "react"
import { useDispatch, useSelector } from "react-redux"
//making sure Redirect imports from react-router-dom and not react-router
import { Link, Redirect } from "react-router-dom"
import AppContainer from "../../hoc/AppContainer"
import { translationAddAction } from "../../store/actions/translationActions"
import styles from "./Translation.module.css"

const Translate = () => {
    //useDispatch() is a hook from react-redux library
    //to dispatch functions
    const dispatch = useDispatch()

    //get loggedIn variable from sessionReducer.js
    const { loggedIn, username } = useSelector((state) => state.sessionReducer)

    //useState() hook receives default state argument
    //using array destructuring to get default credentials and setCredentials from useState()
    //only valid in functional components
    //setting a local state:
    const [translation, setTranslation] = useState("")
    const [signs, setSigns] = useState([])

    const onInputChange = (event) => {
        setTranslation({
            translation: event.target.value,
        })
    }

    const translate = (text) => {
        //do not accept null values or program breaks
        if (text.match(/[a-zA-Z]/gi) !== null) {
            return text
                .match(/[a-zA-Z]/gi)
                .map((char) => `/individual_signs/${char}.png`)
        } else {
            return []
        }
    }

    const onFormSubmit = (event) => {
        //form submission restarts app by default,
        //overriding default behaviour
        event.preventDefault()
        const payload = Object.values(translation)[0]
        setSigns(translate(Object.values(translation)[0]))
        dispatch(translationAddAction([username, payload]))
    }

    return (
        <>
            {/* IF not loggedIn, redirect to start */}
            {!loggedIn && <Redirect to="/" />}

            {/* IF loggedIn, render this */}
            {loggedIn && (
                <AppContainer>
                    {/* form becomes child of AppContainer */}
                    <form className="mt-3 mb-3" onSubmit={onFormSubmit}>
                        {/* mt-3 = bootstrap top margin size 3 */}

                        <h1>Translate text</h1>

                        {/* mb-3 = bootstrap bottom margin size 3 */}
                        {/* jsx syntax class = className */}
                        {/* using bootstrap classes form-label and form-control */}
                        <div className="mb-3">
                            {/* jsx syntax for = htmlFor */}
                            <input
                                id="translation"
                                type="text"
                                placerholder="Enter text to translate"
                                className="form-control"
                                maxLength="40"
                                onChange={onInputChange}
                            />
                        </div>

                        <div>
                            {signs.map((sign, index) => (
                                <div
                                    className={styles.SignContainer}
                                    key={index}
                                >
                                    <img
                                        className={styles.Sign}
                                        src={sign}
                                        alt="sign language"
                                    />
                                </div>
                            ))}
                        </div>

                        {/* button is set to submit the form */}
                        <button
                            type="submit"
                            className="btn btn-primary btn-lg"
                        >
                            Translate
                        </button>
                    </form>
                    <Link to="/profile">
                        <button className="btn btn-primary btn-lg">
                            Profile
                        </button>
                    </Link>
                </AppContainer>
            )}
        </>
    )
}

export default Translate
