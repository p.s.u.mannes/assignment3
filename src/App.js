import "./App.css"
import { BrowserRouter, Switch, Route } from "react-router-dom"
import Login from "./components/Login/Login"
import Translation from "./components/Translation/Translation"
import Profile from "./components/Profile/Profile"
import NotFound from "./components/NotFound/NotFound"
import AppContainer from "./hoc/AppContainer"
import styles from "./App.module.css"

function App() {
    return (
        <BrowserRouter>
            <div className="App">
                <AppContainer>
                    <h1 className={styles.header}>LOST IN TRANSLATION</h1>
                </AppContainer>

                {/* ROUTES RENDERED HERE */}
                <Switch>
                    <Route path="/" exact component={Login} />
                    <Route path="/translation" exact component={Translation} />
                    <Route path="/profile" component={Profile} />

                    {/* Match everything...last in list to catch all links */}
                    <Route path="*" exact component={NotFound} />
                </Switch>
            </div>
        </BrowserRouter>
    )
}

export default App
