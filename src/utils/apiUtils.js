export const handleFirstResponse = async (response) => {
    if (!response.ok) {
        //waiting for promise to be returned with await keyword and default error as fallback message
        const { error = "Unknown error occurred." } = await response.json()
        throw new Error(error)
    }
    return response.json()
}
