//index file for middleware

import { applyMiddleware } from "redux"
import { loginMiddleware } from "./loginMiddleware"
import { sessionMiddleware } from "./sessionMiddleware"
import { translationMiddleware } from "./translationMiddleware"

//does not accept object, accept undefined number of middlewares
export default applyMiddleware(
    loginMiddleware,
    sessionMiddleware,
    translationMiddleware
)
