//middleware takes care of asyncronous code (reducers and actions need to be syncronous)
import { TranslateAPI } from "../../components/Translation/TranslateAPI"
import {
    ACTION_TRANSLATION_GET,
    ACTION_TRANSLATION_ADD,
    ACTION_TRANSLATION_DELETE,
} from "../actions/translationActions"

//middleware is a function that returns a function that returns a function
export const translationMiddleware =
    ({ dispatch }) =>
    (next) =>
    (action) => {
        next(action)

        if (action.type === ACTION_TRANSLATION_GET) {
            TranslateAPI.getTranslation()
        }

        if (action.type === ACTION_TRANSLATION_ADD) {
            TranslateAPI.addTranslation(action.payload)
        }

        if (action.type === ACTION_TRANSLATION_DELETE) {
            TranslateAPI.deleteTranslation()
        }
    }
