import {
    ACTION_SESSION_INIT,
    ACTION_SESSION_SET,
    ACTION_SESSION_LOGOUT,
    sessionSetAction,
} from "../actions/sessionActions"

export const sessionMiddleware =
    ({ dispatch }) =>
    (next) =>
    (action) => {
        next(action)

        if (action.type === ACTION_SESSION_INIT) {
            //read LS (local storage)
            const storedSession = localStorage.getItem("lit-ss")

            //check if session exists
            if (!storedSession) {
                return
            }
            //if session exists, parse stored session back into session JSON object
            const session = JSON.parse(storedSession)

            dispatch(sessionSetAction(session))
        }

        if (action.type === ACTION_SESSION_LOGOUT) {
            localStorage.setItem("lit-ss", " ")
        }

        if (action.type === ACTION_SESSION_SET) {
            //storing session in local state, better would be to use cookies
            //setting item react text forum session (rtxtf-ss)
            localStorage.setItem("lit-ss", JSON.stringify(action.payload))
        }
    }
