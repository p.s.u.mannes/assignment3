//middleware takes care of asyncronous code (reducers and actions need to be syncronous)
import { LoginAPI } from "../../components/Login/LoginAPI"
import {
    ACTION_LOGIN_ATTEMPTING,
    ACTION_LOGIN_SUCCESS,
    loginErrorAction,
    loginSuccessAction,
} from "../actions/loginActions"
import { sessionSetAction } from "../actions/sessionActions"

//middleware is a function that returns a function that returns a function
export const loginMiddleware =
    ({ dispatch }) =>
    (next) =>
    (action) => {
        next(action)

        if (action.type === ACTION_LOGIN_ATTEMPTING) {
            //MAKE HTTP REQUEST to try and login
            LoginAPI.login(action.payload)
                .then((profile) => {
                    //LOGIN SUCCESS
                    dispatch(loginSuccessAction(profile))
                })
                .catch((error) => {
                    //THROW ERROR
                    dispatch(loginErrorAction(error.message))
                })
        }

        if (action.type === ACTION_LOGIN_SUCCESS) {
            dispatch(sessionSetAction(action.payload))
        }
    }
