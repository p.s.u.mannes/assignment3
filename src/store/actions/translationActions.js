export const ACTION_TRANSLATION_GET = "[translation] GET"
export const ACTION_TRANSLATION_ADD = "[translation] ADD"
export const ACTION_TRANSLATION_DELETE = "[translation] DELETE"

export const translationGetAction = () => ({
    type: ACTION_TRANSLATION_GET,
})

export const translationAddAction = (translation) => ({
    type: ACTION_TRANSLATION_ADD,
    payload: translation,
})

export const translationDeleteAction = () => ({
    type: ACTION_TRANSLATION_DELETE,
})
