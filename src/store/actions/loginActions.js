//declaring action names, useful for redux devtool debugging
export const ACTION_LOGIN_ATTEMPTING = '[login] ATTEMPT'
export const ACTION_LOGIN_SUCCESS = '[login] SUCCESS'
export const ACTION_LOGIN_ERROR = '[login] ERROR'

export const loginAttemptAction = credentials => ({
    //tying action name to type property of action
    type: ACTION_LOGIN_ATTEMPTING,
    payload: credentials
})

export const loginSuccessAction = profile => ({
    //tying action name to type property of action
    type: ACTION_LOGIN_SUCCESS,
    payload: profile
})

export const loginErrorAction = error => ({
    //tying action name to type property of action
    type: ACTION_LOGIN_ERROR,
    payload: error
})