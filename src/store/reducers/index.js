//root reducer, root of application exists here
//all reducers combined into one

import { combineReducers } from "redux"
import { loginReducer } from "./loginReducer"
import { sessionReducer } from "./sessionReducer"
import { translationReducer } from "./translationReducer"

//appReducer also known as rootReducer
const appReducer = combineReducers({
    loginReducer,
    sessionReducer,
    translationReducer,
})

export default appReducer
