import {
    ACTION_TRANSLATION_GET,
    ACTION_TRANSLATION_ADD,
    ACTION_TRANSLATION_DELETE,
} from "../actions/translationActions"

const initialState = {
    translationLog: [],
}

export const translationReducer = (state = initialState, action) => {
    switch (action.type) {
        case ACTION_TRANSLATION_GET:
            return {
                ...state,
            }

        case ACTION_TRANSLATION_ADD:
            return {
                ...state,
                translationLog: action.payload,
            }

        case ACTION_TRANSLATION_DELETE:
            return {
                initialState,
            }

        default:
            return state
    }
}
