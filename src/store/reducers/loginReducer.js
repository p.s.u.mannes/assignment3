//reducer - place in code where store is told what new state looks like

import {
    ACTION_LOGIN_ATTEMPTING,
    ACTION_LOGIN_ERROR,
    ACTION_LOGIN_SUCCESS,
} from "../actions/loginActions"

const initialState = {
    loginAttemping: false,
    loginError: "",
}

export const loginReducer = (state = initialState, action) => {
    switch (action.type) {
        case ACTION_LOGIN_ATTEMPTING:
            return {
                ...state,
                loginAttemping: true,
                loginError: "",
            }
        case ACTION_LOGIN_SUCCESS:
            return {
                ...initialState,
            }
        case ACTION_LOGIN_ERROR:
            return {
                ...state,
                loginAttemping: false,
                loginError: action.payload,
            }
        default:
            return state
    }
}
