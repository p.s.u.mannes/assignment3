# assignment3

**LOST IN TRANSLATION**

This app is designed to translate user input 
into sign language and stores the inputs in a database.
Usernames are stored in local storage and are used to make an account.
This single-page application comes with a login, translation, and profile page.

**The app requires the database to run in a JSON server:**

_npx json-server --watch data/translation.json --port 8000_

Running the app: 
_npm start_

Limitations:
1. The app does not render a list of the recent 10 values
as intended and clearing the profile page functionality can
be made more user-friendly
2. Additional form checks may be useful.













